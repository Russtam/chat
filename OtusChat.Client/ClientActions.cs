﻿using ChatHelper;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace OtusChat.Client
{
    public class ClientActions : IClientActions, IDisposable
    {
        private readonly string _address;
        private readonly int _port;
        private readonly TcpClient _client;

        private NetworkStream _stream;
        public ClientActions(string address, int port)
        {
            _client = new TcpClient();
            _address = address;
            _port = port;
        }
        /// <summary>
        /// Создание канала для получения данных
        /// </summary>
        /// <param name="name">имя</param>
        /// <param name="settings">настройки хоста</param>
        public void Connection(string name)
        {
            try
            {
                _client.Connect(_address, _port);
                _stream = _client.GetStream();

                string message = name;
                byte[] data = Encoding.Unicode.GetBytes(message);
                _stream.Write(data, 0, data.Length);

                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread?.Start();

                Console.WriteLine($"Добро пожаловать, {name}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Ошибка при подключении.");
            }
        }

        /// <summary>
        /// Закрытие подключения
        /// </summary>
        public void Disconnect()
        {
            _stream?.Close();
            _client?.Close();
            Dispose();
        }

        public void Dispose()
        {
            GC.Collect();
            Environment.Exit(0);
        }

        /// <summary>
        /// Получение сообщений
        /// </summary>
        public void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[1024];
                    Message msg = new Message();
                    int bytes = 0;
                    do
                    {
                        bytes = _stream.Read(data, 0, data.Length);
                        var line = Encoding.Unicode.GetString(data, 0, bytes);
                        msg = line.Deserialize();
                    }
                    while (_stream.DataAvailable);

                    Console.WriteLine(msg.ToString());
                }
                catch
                {
                    Disconnect();
                    Console.WriteLine("Подключение прервано!");
                }
            }
        }

        /// <summary>
        /// Отправка сообщений
        /// </summary>
        public void SendMessage(string msg)
        {
            try
            {
                var byteMsg = Encoding.Default.GetBytes(msg);
                _stream.Write(byteMsg);
            }
            catch (TimeoutException)
            {
                Console.WriteLine("Сервер временно не доступен!");
            }
        }
    }
}
