﻿using ChatHelper;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtusChat.Client
{
    public interface IClientActions
    {
        void Disconnect();
        void SendMessage(string message);
        void ReceiveMessage();
        void Connection(string name);
    }
}
