﻿using ChatHelper;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace OtusChat.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IConfiguration config = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json").Build();
                var settings = config.GetSection("host").Get<Settings>();

                IClientActions client = new ClientActions(settings.Address, settings.Port);
                Console.WriteLine("Представьтесь, пожалуйста:");
                var name = Console.ReadLine();

                client.Connection(name);
                while (true)
                {
                    var msg = Console.ReadLine();
                    client.SendMessage(msg);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
            }
        }
    }
}
