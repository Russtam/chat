﻿
namespace ChatHelper
{
    public class Message
    {
        public string Text { get; set; }
        public string UserName { get; set; }
        public MessageType MsgType { get; set; }

        public override string ToString()
        {
            switch (MsgType)
            {
                case MessageType.Mail:
                    return $"{UserName}: {Text}";
                case MessageType.ServiceMail:
                    return Text;
                    
            }
            return $"Error {Text}";
        }
    }
}
