﻿using System;

namespace ChatHelper
{
    public class Settings
    {
        public string Address { get; set; }
        public int Port { get; set; }
    }
}
