﻿using System.Text.Json;

namespace ChatHelper
{
    public static class MessageSerializeExtensions
    {
        public static Message Deserialize(this string text)
        {
            return JsonSerializer.Deserialize<Message>(text);
        }

        public static string Serialize(this Message message)
        {
           return JsonSerializer.Serialize(message);
        }
    }
}
