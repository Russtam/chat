﻿using ChatHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace OtusChat.Server.Core
{
    public class ServerApp : IServerApp
    {
        private static TcpListener _listener;
        List<Client> clients = new List<Client>();
        public void AddConnection(Client clientObj)
        {
            clients.Add(clientObj);
        }
        public ServerApp(string ipAddress, int port)
        {
            _listener = new TcpListener(IPAddress.Parse(ipAddress), port);
        }
        public ServerApp() { }

        public void BroadcastMessage(Message message, string id, bool isError=false)
        {
            byte[] data = Encoding.Unicode.GetBytes(message.Serialize());
            if (!isError)
            {
                for (int i = 0; i < clients.Count; i++)
                {
                    if (clients[i].Id != id)
                    {
                        clients[i].Stream.Write(data, 0, data.Length);
                    }
                }
            }
            else
            {
                var client = clients.Single(c => c.Id == id);
                client.Stream.Write(data, 0, data.Length);
            }
        }

        public void Disconnect()
        {
            _listener.Stop();
            clients.ForEach(client => client.Close());
            Environment.Exit(0);
        }

        public void Listen()
        {
            try
            {
                if (_listener == null)
                    throw new ArgumentNullException("Канал для прослушивания не был создан");
                _listener.Start();
                Console.WriteLine("Server started");

                while (true)
                {
                    TcpClient tcpClient = _listener.AcceptTcpClient();
                    Client clientObject = new Client(tcpClient, this);
                    AddConnection(clientObject);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread?.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        public void RemoveConnection(string id)
        {
            var client = clients.Single(x => x.Id == id);
            clients.Remove(client);
        }
    }
}
