﻿using ChatHelper;
using System;
using System.Net.Sockets;
using System.Text;

namespace OtusChat.Server.Core
{
    public class Client
    {
        internal string Id { get; private set; }
        internal string Name { get; }
        internal NetworkStream Stream { get; set; }
        TcpClient _client;
        IServerApp _server;

        public Client(TcpClient client, IServerApp server)
        {
            Id = Guid.NewGuid().ToString();
            _client = client;
            _server = server;
        }

        /// <summary>
        /// Процесс получения сообщений от клиентов
        /// </summary>
        public void Process()
        {
            try
            {
                Stream = _client.GetStream();
                var name = GetMessage();
                var mess = $"{name} присоединился к чату";
                Send(mess, MessageType.ServiceMail);

                while (true)
                {
                    try
                    {
                        var text = GetMessage();
                        Send(text, MessageType.Mail, name);
                    }
                    catch
                    {
                        var text = $"{name} Покинул чат";
                        Send(text, MessageType.ServiceMail);
                        break;
                    }
                }
            }
            catch (SocketException ex)
            {
                Send(ex.Message, MessageType.Error);
                
            }
            catch (Exception ex)
            {
                Send(ex.Message, MessageType.Error);
            }
            finally
            {
                _server.RemoveConnection(this.Id);
                Close();
            }
        }

        private void Send(string text, MessageType msgType, string name=null)
        {
            bool isError = msgType != MessageType.Error ? false : true;
            var message = new Message
            {
                MsgType = msgType,
                Text = text,
                UserName = name
            };
            Console.WriteLine(message.ToString());
            _server.BroadcastMessage(message, this.Id, isError);
        }

        private string GetMessage()
        {
            try
            {
                byte[] data = new byte[1024];
                int bytes = 0;
                string msg;
                do
                {
                    bytes = Stream.Read(data, 0, data.Length);
                    msg = Encoding.Default.GetString(data, 0, data.Length).Replace("\0", "");
                } while (Stream.DataAvailable);
                return msg;
            }
            catch 
            {
                return "Ошибка получения сообщения";
            }
        }
        internal void Close()
        {
            Stream?.Close();
            _client?.Close();
        }
    }
}
