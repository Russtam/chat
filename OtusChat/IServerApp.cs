﻿
using ChatHelper;

namespace OtusChat.Server.Core
{
    public interface IServerApp
    {
        void AddConnection(Client client);
        void RemoveConnection(string id);
        void Listen();
        void BroadcastMessage(Message message, string id, bool isError=false);
        void Disconnect();

        
    }
}
