﻿using ChatHelper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using OtusChat.Server.Core;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace OtusChat.Server.App
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IConfiguration config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appSettings.json")
                    .Build();
                var settings = config.GetSection("host").Get<Settings>();
                IServerApp server = new ServerApp(settings.Address, settings.Port);
                var listenerThread = new Thread(new ThreadStart(server.Listen));
                listenerThread?.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
            }
        }
    }
}
